#!/bin/bash
#
POLYBAR_CONF=~/.config/polybar/config

function set_polybar_param(){
    # $1 - filename
    # $2 - module name '\[module\/battery\]'
    # $3 - param
    # $4 - val

    EXISTING=$(cat $1 | grep $2 -A 15 -n | egrep --max-count=1  "$3\s*=.*" | awk -F- '{ print $1 }')
    echo Existing: $EXISTING
    [[ ! -z $EXISTING ]] &&
        sed -i -r "${EXISTING}s/^($3)(.*)/\1 = $4/" $1 ||
        sed -i "/$2/a $3 = $4" $1
}

function ask_question(){
    while true; do
        read -p "$1 Y/n   " yn
        case $yn in
            [Yy]* )
                echo 1
                        return
                ;;
                [Nn]* )
                echo 0
                        return
                ;;

        esac
    done
}

function setup_network() {

    for interface in $(ls /sys/class/net); do
        if [[  $interface == "lo" ]]; then
            continue
        fi

        tput clear
        echo -n -d "Interface: $interface

        [1]   Use as ethernet adapter
        [2]   Use as wifi adapter
        [any] skip

    "

        read -p "" input

        case $input in
            ["1"]* )

                echo "
    Using $interface as ethernet adapter"
                set_polybar_param $POLYBAR_CONF  '\[module\/eth\]' interface $interface
                sleep 1
                ;;

            ["2"]* )
                echo "
    Using $interface as wifi adapter"
                set_polybar_param $POLYBAR_CONF  '\[module\/wlan\]' interface $interface
                sleep 1
                ;;
            *)
                echo "
    Skipping $interface..."
                sleep 1
                ;;

        esac
    done

}


function setup_battery() {

    echo Setting up battery
    for supply in  $(ls -1 /sys/class/power_supply/); do

        tput clear
        echo -n -d "Supply: $supply

        [1]   Use as AC adapter
        [2]   Use as battery
        [any] skip

    "

        read -p "" input

        case $input in
            ["1"]* )

                echo "
    Using $supply as AC adapter"

                set_polybar_param $POLYBAR_CONF  '\[module\/battery\]' adapter $supply

                sleep 1
                ;;

            ["2"]* )
                echo "
    Using $supply as battery"

                set_polybar_param $POLYBAR_CONF  '\[module\/battery\]' battery $supply

                sleep 1
                ;;
            *)
                echo "
    Skipping $supply..."
                sleep 1
                ;;

        esac
    done




}

# Linking config files 
if [[ ! -d ~/.config ]]; then
    mkdir  -p ~/.config
fi


for item in $(ls ./config); do
    if [[ ! -L ~/.config/$item ]]; then
        ln -s $(pwd)/config/$item ~/.config/$item
    fi
done


# Linking local files and scripts

if [[ ! -d ~/.local/bin ]]; then 
    mkdir -p ~/.local
fi


for item in $(ls ./local); do
    if [[ ! -L ~/.local/$item ]]; then
        ln -s $(pwd)/local/$item ~/.local/$item
    fi
done

# Loading tilix config
dconf load /com/gexperts/Tilix/ < tilix.dconf


#ln -d $(pwd)/config/zsh/.zshrc ~/.zshrc
ln -d $(pwd)/config/zsh/.zprofile ~/.zprofile

echo Configuring polybar...
sleep 2


if  [[ $(ask_question "Setup network adapters for polybar? ")  -gt 0 ]] ; then
    setup_network
fi


if  [[ $(ask_question "Power supply adapters for polybar? ")  -gt 0 ]] ; then
    setup_battery
fi
