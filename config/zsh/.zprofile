export ZDOTDIR="$HOME/.config/zsh"

if [[ $- == *i* ]]; then . ~/.zshrc; fi

if [[ ! ${DISPLAY} && ${XDG_VTNR} == 1 ]]; then
     exec startx /usr/bin/i3 
fi

