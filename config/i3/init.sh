#!/usr/bin/env bash

# esc to caps
setxkbmap -option caps:escape



# Keyboard layout
setxkbmap -layout us,ru
setxkbmap -option 'grp:alt_shift_toggle'

# Duplicate mod key in i3
xmodmap -e 'keycode 135 = Super_R' && xset -r 135

# Set wallpapers for all 3 screens
sleep 1
nitrogen --head=0 --set-scaled  ~/.config/i3/wallpaper.jpg
nitrogen --head=1 --set-scaled  ~/.config/i3/wallpaper.jpg
nitrogen --head=2 --set-scaled  ~/.config/i3/wallpaper.jpg
