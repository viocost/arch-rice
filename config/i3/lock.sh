#!/bin/zsh


# i3lock -i $HOME/.config/i3/lock.png -t
#
# xscreensaver-command -lock

gnome-screensaver-command -l

# XSECURELOCK_AUTH_BACKGROUND_COLOR=rgb:0/0/0
# XSECURELOCK_AUTH_FOREGROUND_COLOR=rgb:0/b/0
# XSECURELOCK_SAVER=saver_xscreensaver
#
# xsecurelock

# sleep 1 adds a small delay to prevent possible race conditions with suspend
sleep 1

exit 0
